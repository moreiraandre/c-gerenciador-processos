#include <stdio.h>
#include "GerenciadorProcessos.h"

void novaFila(PGP g) {
    g->tam = 0;
    g->inicio = g->fim = -1;
    int x = 0;
    while (x++ < MAX_PRIORIDADES)
        g->escalonados[x] = 0;
}

int filaVazia(PGP g) {
    return (g->tam == 0) ? 1 : 0;
}

int filaCheia(PGP g) {
    return (g->tam == MAX_FILA) ? 1 : 0;
}

int inserir(PGP g, int elemento) {
    if (filaCheia(g)) return 0;
    
    g->fim = (g->fim+1) % MAX_FILA;
    g->fila[g->fim] = elemento;
    // NECESSÁRIO QUANDO INSERIDO O PRIMEIRO ELEMENTO P/ POSIOCIONAR O INÍCIO, NESSE PONTO INICIO=FIM
    if (g->inicio == -1)
        g->inicio = 0;

    g->tam++;
    
    return 1;
}

int escalonar(PGP g, int prioridade) {
    int achou;
    // E PRIORIDADE NÃO É A DE MENOR IMPORTÂNCIA
    if (prioridade != 1) { // VERIFIQUE SE EXISTE ALGUMA PRIORIDADDE DE MENOR IMPORTÂNCIA ESCALONADA PARA QUE A NOVA PRIORIDADE SEJA ESCALONADA ANTES
        //printf("aqui1");
        int x = prioridade;
        achou = 0;
        while ((--x > 0) && (achou == 0)) {
            if (g->escalonados[x] > 0) {
                achou = 1;
                x++;
            }
        }
                
        if (achou == 0) // INSERE NO FIM DA LISTA
            inserir(g, prioridade);
        else {
            //printf("aqui2 fim: %i\n", g->fim);
            int y = g->fim + 1;
            achou = 0;
            //printf("-:> x: %i, g->fila[--y]: %i, prioridade: %i\n", x, g->fila[y-1], prioridade);
            while (g->fila[--y] != x) { // ENCONTRE A PRIMEIRA OCORRENCIA DA PRIORIDADE DE MENOR IMPORTÂNCIA
                g->fila[y+1] = g->fila[y];
                
                //printf("\nprioridade: %i, x: %i, y: %i, y+1: %i, g[y]: %i, g[y+1]: %i\n", prioridade, x, y, y+1, g->fila[y], g->fila[y+1]);
            }
            g->fila[y+1] = g->fila[y];
            
            if (y == g->inicio)
                g->fila[y] = prioridade;
            else {
                y++;
                //printf("aqui9\n");
                //printf("g->fila[--y]: %i, x: %i, y: %i, g->inicio: %i\n", g->fila[y-1], x, y, g->inicio);
                while ((g->fila[--y] == x) && (y != g->inicio)) { // ENCONTRE A PRIMEIRA OCORRENCIA DA PRIORIDADE DE MENOR IMPORTÂNCIA
                    g->fila[y+1] = g->fila[y];
                    //printf("...g->fila[--y]: %i, x: %i, y: %i, g->inicio: %i\n", g->fila[y-1], x, y, g->inicio);
                }
                g->fila[y+1] = prioridade;
            }
            if (g->inicio == -1)
                g->inicio = 0;
            g->fim = (g->fim+1) % MAX_FILA;
            g->tam++;
            
        }
    } else
        inserir(g, prioridade);

    if (g->escalonados[prioridade] == MAX_ESCALONAMENTOS - 1)
        g->escalonados[prioridade] = 0;
    else
        g->escalonados[prioridade]++;
}

int removerFila(PGP g, int *el) {
    if (filaVazia(g)) return 0;
    
    *el = g->fila[g->inicio];
    g->tam--;
    g->inicio = (g->inicio+1) % MAX_FILA;
    return 1;
}

int get(PGP g, int posicao, int *el) {
    if (posicao > g->fim) return 0;

    *el = g->fila[(g->inicio + posicao) % MAX_FILA];
    
    return 1;
}
