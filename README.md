# Gerenciador de Processos em _C_

# Clonar repositório
```
git clone https://gitlab.com/moreiraandre/c-gerenciador-processos.git
```

# Compilar
```
cd c-gerenciador-processos
make
```

# Executar
```
./principal
```
