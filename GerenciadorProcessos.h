#define MAX_FILA 100
#define MAX_ESCALONAMENTOS 3
#define MAX_PRIORIDADES 3

typedef struct {
    int fila[MAX_FILA];
    int tam, inicio, fim;
    
    // CONTROLA QUANTAS VEZES CADA PRIORIDADE FOI ESCALONADA
    // escalonados[<PRIORIDAE>] = <QTD_ESCALONAMENTOS>
    int escalonados[MAX_PRIORIDADES];
} GP; // GERENCIADOR DE PROCESSOS

typedef GP *PGP;

void novaFila(PGP);

int filaVazia(PGP);

int filaCheia(PGP);

int inserir(PGP, int);

int escalonar(PGP, int);

int removerFila(PGP, int *);

int get(PGP, int, int *);
