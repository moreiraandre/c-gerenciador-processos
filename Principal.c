#include <stdio.h>
#include <stdlib.h>
#include "GerenciadorProcessos.h"

GP Gerenciador;

void imprimirFila() {
    if (Gerenciador.tam == 0)
        printf("FILA VAZIA!\n");
    else {
        int x = 0;
        int el;
        printf("FILA\n");
        while (get(&Gerenciador, x++, &el) != 0)
            printf("%i|", el);
        printf("\n");
    }
}

int main() {
    novaFila(&Gerenciador);

    int op;
    
    do {
        system("clear");
        imprimirFila();
        printf("1=BAIXA 2=MEDIA 3=ALTA 0=SAIR\n");
        printf("INFORME A PRIORIDADE E PRESSIONE <ENTER> PARA ESCALONAR O PROCESSO: ");
        scanf("%i", &op);
        if (op != 0)
            escalonar(&Gerenciador, op);
    } while(op != 0);
    
    return 0;
}
